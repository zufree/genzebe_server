/* eslint-disable promise/always-return */
import express from "express";
import ApiApp from "./api_app";
import {
  developmentErrors,
  notFound,
  productionErrors
} from "./core/error_handlers";
import brokerDb from "./models";
const morgan = require("morgan");
const compression = require('compression')
const helmet = require('helmet');
const fs = require('fs');
const app = express();


/**
 * Get NODE_ENV from environment and store in Express.
 */
app.set('env', 'production');
// process.env.NODE_ENV = 'production';

/**
 * When running Express app behind a proxy we need to detect client IP address correctly.
 * For NGINX the following must be configured 'proxy_set_header X-Forwarded-For $remote_addr;'
 * @link http://expressjs.com/en/guide/behind-proxies.html
 */
app.set('trust proxy', true);
// console.log(process.env.NODE_ENV);
app.use(compression());
app.use(helmet());


brokerDb.sequelize
  .sync()
  .then(async () => {
    // dailyJobToActiveEqub()
    // 
    console.log("Connection has been established successfully.");
  })
  .catch((error) => {
    console.error("Unable to connect to the database:", error);
  });

// app.use(morgan('dev', {
//   stream: fs.createWriteStream('./access.log', {flags: 'a'})
// }));
app.use(morgan('dev'))

app.use(async(req, res, next) => {

  next();
})

app.use(`/api`, ApiApp);


// If that above routes didnt work, we 404 them and forward to error handler
app.use(notFound);


// everySundayJobJudgeWinner()

// Otherwise this was a really bad error we didn't expect! Shoot eh
if (app.get("env") === "development") {
  app.use(developmentErrors);
}

// production error handler
app.use(productionErrors);

app.set("port", 1077);
const server = app.listen(app.get("port"), () => {
  console.log(`Express running → PORT ${server.address().port}`);
  console.log(process.env.NODE_ENV);
});
