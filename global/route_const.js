class RouteConst {
  constructor() {
    this.post = '/post';
    this.user = '/user';
    this.admin = '/admin';
  }
}

const routeConst = new RouteConst();
export default routeConst;