// Ctrl+Alt+D and again Ctrl+Alt+D

/**
	* replaceAt('13252864200', 3, '****')
 */
export const replaceAt = (string, index, replacement) => string.substr(0, index) + replacement + string.substr(index + replacement.length);


/**
 *
 * @param {string} phone - phone number
 * @param {number} length - length of the replacement *
 * @example
 * phoneFormat('13267229922')
 */
export const phoneFormat = (phone, length) => replaceAt(phone.toString(), 3, '*'.repeat(length));
// const phoneArray = [...phone];
// let str = '';
// phoneArray.forEach((item, index) => {
// 	if (index === 3 || index === 4 || index === 5 || index === 6 || index === 7 || index === 8) {
// 		str += '*';
// 	} else {
// 		str += item;
// 	}
// });
// return str;
/**
 *
 *
 * @param {string} txt
 * @param {string} literal
 * @param {string} join
 * @returns {Promise<string>}
 */
export const convertLetterToNumber = (txt='', literal, join) => {
	// eslint-disable-next-line no-param-reassign
	if (!literal) txt = txt.toLowerCase();
	return txt.split('').map(c => 'abcdefghijklmnopqrstuvwxyz'.indexOf(c) + 1 || (literal ? c : '')).join(join || '');
};

// console.log(this.convertLetterToNumber('hello', null, ' '));

export const convertNumberToLetter = num => String.fromCharCode(num + 64);

// '8/5/12/12/15'.split('/').forEach((v) => {
// 	console.log(this.convertNumberToLetter(Number(v)));
// });

export const currentDate = () => Date.now();

export const padLeftZero = str => (`00${str}`).substr(str.length);


/**
 *
	new Date().getTime()
	let date = new Date(1554647023445)
	formatDate(date, 'yyyy-MM-dd hh:mm')
	=> 2019-04-07 22:23
*/
export const formatDate = (date=Date.now(), fmt='yyyy-MM-dd hh:mm') => {
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(
			RegExp.$1,
			(`${date.getFullYear()}`).substr(4 - RegExp.$1.length),
		);
	}
	const o = {
		'M+': date.getMonth() + 1,
		'd+': date.getDate(),
		'h+': date.getHours(),
		'm+': date.getMinutes(),
		's+': date.getSeconds(),
	};
	for (const k in o) {
		if (new RegExp(`(${k})`).test(fmt)) {
			const str = `${o[k]}`;
			fmt = fmt.replace(
				RegExp.$1,
				RegExp.$1.length === 1 ? str : this.padLeftZero(str),
			);
		}
	}
	return fmt;
};

export const randomNumber = () => (new Date().getUTCMilliseconds().toString() + new Date().getTime().toString()).toString();

export const toNumber = number => Number(number);


/**
 * timeAgo(new Date(Number('1580344653925'))) // must be number
 * timeAgo(new Date('2020-01-30T00:51:14.172Z'))
 * const aDay = 24 * 60 * 60 * 1000;
console.log(timeAgo(new Date(Date.now() - aDay)));
 console.log(timeAgo(new Date(Date.now() - aDay * 2)));
*/
export const timeAgo = (date) => {
	const seconds = Math.floor((new Date() - date) / 1000);

	let interval = Math.floor(seconds / 31536000);

	if (interval > 1) {
		return `${interval} years`;
	}
	interval = Math.floor(seconds / 2592000);
	if (interval > 1) {
		return `${interval} months`;
	}
	interval = Math.floor(seconds / 86400);
	if (interval > 1) {
		return `${interval} days`;
	}
	interval = Math.floor(seconds / 3600);
	if (interval > 1) {
		return `${interval} hours`;
	}
	interval = Math.floor(seconds / 60);
	if (interval > 1) {
		return `${interval} minutes`;
	}
	return `${Math.floor(seconds)} seconds`;
};

export const randomArray = (n, l) => {
	const rnd = [];
	for (let i = 0; i < n; i += 1) {
		rnd.push(Math.floor(Math.random() * l));
	}
	return rnd;
};


export const removeDuplicates = (myArr, prop) => myArr.filter((obj, pos, arr) => arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos);


export const isDef = value => value !== undefined && value !== null;

export const isDefAndNotEmpty =  v => isDef(v) && v !== '' && v.length > 0;

export const isSameDate = (dateA, dateB) => dateA.toISOString() === dateB.toISOString();


/**
 * Run every second
	setInterval(updateCountdown, 1000);
 */
// Update countdown time
function updateCountdown() {
	const currentYear = new Date().getFullYear();

	const newYearTime = new Date(`January 01 ${currentYear + 1} 00:00:00`);
	const currentTime = new Date();
	const diff = newYearTime - currentTime;

	const d = Math.floor(diff / 1000 / 60 / 60 / 24);
	const h = Math.floor(diff / 1000 / 60 / 60) % 24;
	const m = Math.floor(diff / 1000 / 60) % 60;
	const s = Math.floor(diff / 1000) % 60;

	console.log(`${d} days ${h < 10 ? `0${h}` : h} hours ${m < 10 ? `0${m}` : m} minutes ${s < 10 ? `0${s}` : s} seconds`);
}

// eslint-disable-next-line no-sparse-arrays
const is = (type, val) => ![, null].includes(val) && val.constructor === type;


export const ratingCalculator = (data, label) => {
	let large = 0;
	const one = data.filter(item => item[label] === 1).length;
	const two = data.filter(item => item[label] === 2).length;
	const three = data.filter(item => item[label] === 3).length;
	const four = data.filter(item => item[label] === 4).length;
	const five = data.filter(item => item[label] === 5).length;

	large = Math.max.apply(null, [one, two, three, four, five]); // large rating num
	const ratings = [one, two, three, four, five].reduce((a, b) => a + b, 0); // sum of ratings

	return {
		rates: ((5 * five + 4 * four + 3 * three + 2 * two + Number(one)) / (five + four + three + two + one)).toFixed(2),
		weight: {
			one: toNumber((one / ratings).toFixed(2)), // length / sum
			two: toNumber((two / ratings).toFixed(2)),
			three: toNumber((three / ratings).toFixed(2)),
			four: toNumber((four / ratings).toFixed(2)),
			five: toNumber((five / ratings).toFixed(2)),
		},
		ratings,
	};
};


// jsdoc https://devdocs.io/jsdoc-tags/
/**
 * This is a description of the foo function.
 *
 * @constructor
 * @param {string} title - The title of the book.
 * @param {string} author - The author of the book.
 * 
 * See {@link MyClass} and [MyClass's foo property]{@link MyClass#foo}.
 * Also, check out {@link http://www.google.com|Google} and
 * {@link https://github.com GitHub}.
 * @param {SHOE_COLORS} color - The shoe color. Must be an enumerated
 * value of {@link SHOE_COLORS}.
 */