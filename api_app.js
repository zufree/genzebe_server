import bodyParser from 'body-parser';
import express from 'express';
import ApiRoute from './routes/api_routes';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(async (req, res, next) => {
	res.locals.user = req.user || null;
	next();
});

ApiRoute(app)

export default app;
