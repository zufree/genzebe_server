import routeConst from "../global/route_const";
import adminRoute from './app/admin_route';
import postsRoute from './app/posts_route';
import userRoute from './app/user_route';

export default (app) => {
	app.use(routeConst.post, postsRoute);
	app.use(routeConst.user, userRoute);
	app.use(routeConst.admin, adminRoute);
};