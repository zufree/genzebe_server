import express from 'express';
import { editProfile, getMySubmittedPosts, getProfileInfo, loginUser } from '../../controller/app/user_ctr';
import { catchErrors } from '../../core/error_handlers';

const router = express.Router();

router.post('/loginUser', catchErrors(loginUser));
// router.post('/getNotifications', catchErrors(getNotifications));
router.post('/getProfileInfo', catchErrors(getProfileInfo));
router.post('/editProfile', catchErrors(editProfile));
router.post('/getMySubmittedPosts', catchErrors(getMySubmittedPosts));

export default router;