import express from 'express';
import { changePostStatus, changeUsersStatus, deletePost, deleteUser, getAllNotification, getAllPosts, getAllUsers, sendFcmNotification } from '../../controller/app/admin_ctr';
import { catchErrors } from '../../core/error_handlers';

const router = express.Router();

router.post('/changePostStatus', catchErrors(changePostStatus));
router.post('/getAllPosts', catchErrors(getAllPosts));
router.post('/deletePost', catchErrors(deletePost));
router.post('/getAllUsers', catchErrors(getAllUsers));
router.post('/deleteUser', catchErrors(deleteUser));
router.post('/changeUsersStatus', catchErrors(changeUsersStatus));
router.post('/getAllNotification', catchErrors(getAllNotification));
router.post('/sendFcmNotification', catchErrors(sendFcmNotification));


export default router;