import express from 'express';
import { editPost, getPaginatedPost, getPaginatedSearchPost, submitComment, submitNewPost } from '../../controller/app/posts_ctr';
import { catchErrors } from '../../core/error_handlers';

const router = express.Router();

router.post('/submitNewPost', catchErrors(submitNewPost));
router.post('/getPaginatedPost', catchErrors(getPaginatedPost));
router.post('/submitComment', catchErrors(submitComment));
router.post('/editPost', catchErrors(editPost));
router.post('/getPaginatedSearchPost', catchErrors(getPaginatedSearchPost));

export default router;