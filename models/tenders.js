'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tenders extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  tenders.init({
    tenS: DataTypes.DATE,
    tenE: DataTypes.DATE,
    hId: DataTypes.INTEGER,
    cId: DataTypes.INTEGER,
    tenT: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'tenders',
  });
  return tenders;
};