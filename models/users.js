'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  users.init({
    un: DataTypes.STRING,
    pn: DataTypes.STRING,
    tk: DataTypes.STRING,
    pf: DataTypes.STRING,
    dt: DataTypes.STRING,
    st: DataTypes.STRING,
    tp: DataTypes.STRING,
    brId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'users',
  });
  return users;
};