'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class specifications extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  specifications.init({
    fi: DataTypes.STRING,
    va: DataTypes.STRING,
    psId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'specifications',
  });
  return specifications;
};