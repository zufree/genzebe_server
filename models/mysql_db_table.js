class MySqlDbTables {
  constructor(){
    this.posts ='posts';
    this.specifications ='specifications';
    this.images ='images';
    this.users ='users';
    this.comments ='comments';
    this.houses ='houses';
    this.cars ='cars';
    this.jobs ='jobs';
    this.tenders ='tenders';
    this.notifications ='notifications';
  }
}

class MysqlCommonModel  {
  constructor(){
    this.id = 'id';
    this.postId = 'psId';
    this.houseId = 'hId';
    this.jobId = 'jId';
    this.carId = 'cId';
    this.brokerId = 'brId';
    this.tenderId = 'tenId';
    this.realStateId = 'rsId';
    this.createdAt = 'createdAt';
    this.updatedAt = 'updatedAt';
  }
}

const mySqlDbTables = new MySqlDbTables();
const mysqlCommonModel = new MysqlCommonModel();

export { mySqlDbTables, mysqlCommonModel };
