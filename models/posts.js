'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class posts extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  posts.init({
    pr: DataTypes.DOUBLE,
    na: DataTypes.STRING,
    de: DataTypes.TEXT,
    brId: DataTypes.INTEGER,
    lo: DataTypes.STRING,
    la: DataTypes.DOUBLE,
    ln: DataTypes.DOUBLE,
    ty: DataTypes.STRING,
    paT: DataTypes.STRING,
    nt: DataTypes.TEXT,
    st: DataTypes.STRING,
    cod: DataTypes.STRING,
    hId: DataTypes.INTEGER,
    jId: DataTypes.INTEGER,
    cId: DataTypes.INTEGER,
    tenId: DataTypes.INTEGER,
    isIW: {
      type: DataTypes.BOOLEAN,
      allowNull: false, 
      defaultValue: false,
    },
  }, {
    sequelize,
    modelName: 'posts',
  });
  return posts;
};