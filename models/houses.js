'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class houses extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  houses.init({
    sq: DataTypes.STRING,
    bdr: DataTypes.INTEGER,
    btr: DataTypes.INTEGER,
    isRS: DataTypes.STRING,
    advP: DataTypes.INTEGER,
    perT: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'houses',
  });
  return houses;
};