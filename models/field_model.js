
class PostsSqlModel {
  constructor() {

    /// general
    this.price = 'pr';
    this.name = 'na';
    this.desc = 'de';
    this.location = 'lo';
    this.lat = 'la';
    this.lng = 'ln';

    /// PostType
    this.type = 'ty';

    /// PaymentType
    this.paymentType = 'paT';
    this.note = 'nt';
    /// PostStatus
    this.status = 'st';
    this.condition = 'cod'
    this.images = 'images';
    this.comments = 'comments';
    this.specification = 'specifications';
    
    /// house
    this.square = 'sq';
    this.houseBedroom = 'bdr';
    this.houseBathroom = 'btr';
    this.perType = 'perT';
    
    /// car
    this.carYear = 'yr';
    this.carMilage = 'mil';
    this.carColor = 'color';
    this.carDoors = 'door';
    this.carFuelType = 'fut';
    
    /// job
    this.jobEmployeeType = 'ept';
    this.jobWorkExperience = 'wex';
    this.jobEducationLevel = 'edl';

    // tender
    this.tenderStart = 'tenS';
    this.tenderEnd = 'tenE';
    this.tenderType = 'tenT'; /// tenderType

    // realState
    this.advancePayment = 'advP';
    this.isRealState = 'isRS';

    /// what i want
    this.isIWant = 'isIW';
  }
}

class PostStatus {
  constructor(){ 
    this.waiting = '1';
    this.accepted = '2';
    this.rejected = '3';
    this.completed = '4';
  }
}

class PostType {
  constructor() {
    this.rentHouse = '1';
    this.saleHouse = '2';
    this.saleCar = '3';
    this.jobs = '4';
    this.rentCar = '5';
    this.tenders = '6';
    this.realState = '7';
    this.whatIWant = '8';
  }
}

class TenderType {
  constructor() {
    this.house = '1';
    this.car = '2';
  }
}


class PaymentType {
  constructor() {
    this.fixed = '1';
    this.negotiable = '2';
  }
}

class SpecificationModel {
  constructor() {
    this.field = 'fi';
    this.value = 'va';
    // psId
  }
}

class ImagesModel {
  constructor() {
    this.url = 'url';
    // psId
  }
}
class CommentsModel {
  constructor() {
    this.price = 'pr';
    this.body = 'bd';
    // psId
    // brId
  }
}

class UserModelField {
  constructor() {
    this.username = 'un';
    this.phoneNumber = 'pn';
    this.token = 'tk';
    this.platform = 'pf';
    this.status = 'st';

    //  brId

    /// UserType
    this.type = 'tp';
  }
}

class UserStatus {
  constructor() {
    this.waiting = '1';
    this.confirmed = '2';
    this.rejected = '3';
  }
}

class UserType {
  constructor() {
    this.admin = '1';
    this.broker = '2';
    this.customer = '3';
  }
}

class CommonSpecification {
  constructor() {
    this.houseBedroom = 'house~1';
    this.houseBathroom = 'house~2';
    this.jobEmployeeType = 'job~1';
    this.jobWorkExperience = 'job~2';
    this.jobEducationLevel = 'job~3';
    this.carYear = 'car~1';
    this.carMilage = 'car~2';
    this.carColor = 'car~3';
    this.carDoors = 'car~4';
    this.carFuelType = 'car~5';
  }
}

class NotificationField {
  constructor() {
    this.body = 'bd';
    this.title =  'ti';
    this.icon =  'ico';
    // brId
    // psId
  }
  
}

const postsSqlModel = new PostsSqlModel();
const postType = new PostType();
const paymentType = new PaymentType();
const specificationModel = new SpecificationModel();
const imagesModel = new ImagesModel();
const commentsModel = new CommentsModel();
const userModelField = new UserModelField();
const userType = new UserType();
const postStatus = new PostStatus();
const userStatus = new UserStatus();
const commonSpecification = new CommonSpecification();
const notificationField = new NotificationField();
const tenderType = new TenderType();

export {
  postsSqlModel,
  postType,
  paymentType,
  specificationModel,
  imagesModel,
  commentsModel,
  userModelField,
  userType,
  postStatus,
  userStatus,
  commonSpecification,
  notificationField,
  tenderType



};

