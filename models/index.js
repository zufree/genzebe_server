'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];
const brokerDb = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    if(!file.includes('_')) {
      const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
      brokerDb[model.name] = model;
    }
  });

Object.keys(brokerDb).forEach(modelName => {
  if (brokerDb[modelName].associate) {
    brokerDb[modelName].associate(brokerDb);
  }
});

brokerDb.sequelize = sequelize;
brokerDb.Sequelize = Sequelize;

export default brokerDb;

export { sequelize };
