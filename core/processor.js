class Utils {
	ErrMsg(res, option = {
		Code, msg, status, subCode,total, err
	}) {
		return res.status(option.status ? option.status : 200).send({
			Code: option.Code ? option.Code : -1,
			status: option.status ? option.status : 200,
			Msg: option.msg ? option.msg : 'Parameter error',
			Data: null,
			total: option.total ? option.total : 0,
			subCode: option.subCode ? option.subCode : null,
			err: option.err ? option.err : null,
		});
	}

	SuccessMsg(res, option = {
		msg, data, total, subCode,
	}) {
		return res.status(option.status ? option.status : 200).send({
			Code: 0,
			Msg: option.msg ? option.msg : 'ok',
			status: option.status ? option.status : 200,
			Data: option.data ? option.data : [],
			total: option.total ? option.total : 0,
			subCode: option.subCode ? option.subCode : null,
		});
	}

	EmptyObj(res, option = {
		Code, msg, status, data, subCode,total
	}) {
		console.log(option.data);
		return res.status(option.status ? option.status : 200).send({
			Code: option.Code ? option.Code : 0,
			// status: option.status ? option.status : status.NO_CONTENT,
			status: option.status ? option.status : 200,
			Msg: option.msg ? option.msg : '',
			Data: option.data || null,
			total: option.total ? option.total : 0,
			subCode: option.subCode ? option.subCode : null,
		});
	}

	ReturnMessage(res, data) {
		if (data) {
			return this.SuccessMsg(res, {});
		}
		return this.ErrMsg(res, { msg: 'The operation has been failed please try again' });
	}
}

const utils = new Utils();
export default utils;


/*

Info: true
Message: "Thanks for the feedback! Votes cast by those with less than 15 <a href="/help/whats-reputation">reputation</a> are recorded, but do not change the publicly displayed post score."
NewScore: 4
Refresh: false
Success: false
Transient: false
Warning: false


 */
