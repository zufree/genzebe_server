import createError from "http-errors";
import { customLogger } from "../loger";
import utils from "./processor";

/*
  Catch Errors Handler

  With async/await, you need some way to catch errors
  Instead of using try{} catch(e) {} in each controller, we wrap the function in
  catchErrors(), catch any errors they throw, and pass it along to our express middleware with next()
*/

export const catchErrors = (fn) => {
  return function (req, res, next) {
    return fn(req, res, next).catch(next);
  };
};

/*
  Not Found Error Handler

  If we hit a route that is not found, we mark it as 404 and pass it along to the next error handler to display
*/
export const notFound = (req, res, next) => {
  next(createError(404));
};

/*
  Development Error Handler

  In development we show good error messages so if we hit a syntax error or any other previously un-handled error, we can show good info on what happened
*/
export const developmentErrors = (err, req, res, next) => {
  err.stack = err.stack || "";
  const errorDetails = {
    message: err.message,
    // status: err.status || 500,
    status: 200,
    stackHighlighted: err.stack.replace(
      /[a-z_-\d]+.js:\d+:\d+/gi,
      "<mark>$&</mark>"
    ),
  };
  customLogger.info(`🔥 ${err}`);
  // console.log(err);
  return utils.ErrMsg(res, {
    msg: errorDetails.message,
    status: errorDetails.status,
    err: err,
  });
};

/*
  Production Error Handler

  No stacktraces are leaked to user
*/
export const productionErrors = (err, req, res, next) => {
  customLogger.info(`🔥 ${err}`);
  // console.log(err);
  return utils.ErrMsg(res, {
    msg: err.message,
    err: err,
    // status: err.status || 500,
    status: 200,
  });
};



/* 
 "currentPostFilter": {
    "status": null,
    "type": "4",
    "priceMin": null,
    "priceMax": null,
    "selectedSort": [  ],
    "condition": null,
    "nearBy": false,
    "houseMinBathroom": null,
    "houseMinBedroom": null,
    "houseMinSquare": null,
    "houseMaxSquare": null,
    "carMinYear": null,
    "carMaxYear": null,
    "carMinKilometer": null,
    "carMaxKilometer": null,
    "carFuelType": null,
    "jobWorkExperience": null,
    "jobEducationLevel": null,
    "jobEmployeeType": null
  },
  "brokerId": 99999


*/