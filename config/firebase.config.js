const firebase =  require('firebase');
const firebaseAdmin = require('firebase-admin');
const serviceAccount = require('../broker-574bd-firebase-adminsdk-npnzl-2ea108e548.json');
firebaseAdmin.initializeApp({
	credential: firebaseAdmin.credential.cert(serviceAccount),
  // databaseURL: "https://fonehmobile.firebaseio.com",
});

const dbFireStore = firebaseAdmin.firestore();
const dbRealTime = firebaseAdmin.firestore();

module.exports = { firebaseAdmin, dbFireStore, dbRealTime, firebase };

