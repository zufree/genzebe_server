
# posts
sequelize model:generate --name posts --attributes pr:DOUBLE,na:STRING,de:TEXT,brId:INTEGER,lo:STRING,la:DOUBLE,ln:DOUBLE,ty:STRING,paT:STRING,nt:TEXT,st:STRING,cod:STRING,hId:INTEGER,jId:INTEGER,cId:INTEGER,tenId:INTEGER,rsId:INTEGER --force

# houses
sequelize model:generate --name houses --attributes sq:STRING,bdr:INTEGER,btr:INTEGER,isRS:BOOLEAN --force

# cars
sequelize model:generate --name cars --attributes yr:INTEGER,mil:INTEGER,color:STRING,door:INTEGER,fut:STRING --force

# jobs
sequelize model:generate --name jobs --attributes ept:STRING,wex:STRING,edl:STRING --force

# tenders
sequelize model:generate --name tenders --attributes tenS:DATE,tenE:DATE --force

# realState
sequelize model:generate --name realStates --attributes advP:INTEGER --force

# specifications
sequelize model:generate --name specifications --attributes fi:STRING,va:STRING,psId:INTEGER --force

# images
sequelize model:generate --name images --attributes url:STRING,psId:INTEGER --force

# comments
sequelize model:generate --name comments --attributes pr:DOUBLE,bd:STRING,psId:INTEGER,brId:INTEGER --force

# users
sequelize model:generate --name users --attributes un:STRING,pn:STRING,tk:STRING,pf:STRING,dt:STRING,tp:STRING,brId:STRING,st:STRING --force

# notifications
sequelize model:generate --name notifications --attributes psId:INTEGER,bd:STRING,ti:STRING,ico:STRING,brId:INTEGER --force