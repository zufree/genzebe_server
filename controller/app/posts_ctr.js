import utils from "../../core/processor";
import { optimizeQueryPagination } from "../../global/routeHelpers";
import brokerDb, { sequelize } from "../../models";
import {
  commentsModel,
  imagesModel,
  notificationField,
  postsSqlModel,
  postStatus,
  postType,
  specificationModel,

  tenderType,

  userModelField
} from "../../models/field_model";
import { mysqlCommonModel, mySqlDbTables } from "../../models/mysql_db_table";
import { isDef, isDefAndNotEmpty } from "../../utils/helpers";
import { sendFcmNotificationFn } from "./admin_ctr";
import { saveNotification } from "./general_ctr";

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

export const userAttributes = [userModelField.username, userModelField.phoneNumber, userModelField.type];

export const submitNewPost = async (req, res) => {
  let { postModel, brokerName,isAdmin } = req.body;
  console.log(req.body);

  let data;

  /// jobs
  if (postModel[postsSqlModel.type] === postType.jobs) {
    data = await brokerDb[mySqlDbTables.jobs].create(postModel.job);
    postModel[mysqlCommonModel.jobId] = data.id;
  }

  /// rentHouse, saleHouse, realState
  if (
    postModel[postsSqlModel.type] === postType.rentHouse ||
    postModel[postsSqlModel.type] === postType.saleHouse || postModel[postsSqlModel.type] === postType.realState
  ) {
    data = await brokerDb[mySqlDbTables.houses].create(postModel.house);
    postModel[mysqlCommonModel.houseId] = data.id;
  }

  /// saleCar, rentCar
  if (postModel[postsSqlModel.type] === postType.saleCar || postModel[postsSqlModel.type] === postType.rentCar) {
    data = await brokerDb[mySqlDbTables.cars].create(postModel.car);
    postModel[mysqlCommonModel.carId] = data.id;
  }

  /// tenders
  if (postModel[postsSqlModel.type] === postType.tenders) {
    if(postModel.tender[postsSqlModel.tenderType] === tenderType.house) {

      const houseTender = await brokerDb[mySqlDbTables.houses].create(postModel.house);
      postModel.tender[mysqlCommonModel.houseId] = houseTender[mysqlCommonModel.id];
      
    } else if(postModel.tender[postsSqlModel.tenderType] === tenderType.car)  {

      const carTender = await brokerDb[mySqlDbTables.cars].create(postModel.car);
      postModel.tender[mysqlCommonModel.carId] = carTender[mysqlCommonModel.id];

    }

    data = await brokerDb[mySqlDbTables.tenders].create(postModel.tender);

    postModel[mysqlCommonModel.tenderId] = data.id;
  }

  /// realState
  // if (postModel[postsSqlModel.type] === postType.realState) {
  //   data = await brokerDb[mySqlDbTables.realStates].create(postModel.realState);
  //   postModel[mysqlCommonModel.realStateId] = data.id;
  // }

  const posts = await brokerDb[mySqlDbTables.posts].create(postModel);

  const promises = [];

  if (
    postModel[postsSqlModel.specification] &&
    postModel[postsSqlModel.specification].length > 0
  ) {
    const specificationData = postModel[postsSqlModel.specification].map(
      (e) => ({
        [specificationModel.field]: e[specificationModel.field],
        [specificationModel.value]: e[specificationModel.value],
        [mysqlCommonModel.postId]: posts[mysqlCommonModel.id],
      })
    );

    promises.push(
      brokerDb[mySqlDbTables.specifications].bulkCreate(specificationData)
    );
  }

  if (
    postModel[postsSqlModel.images] &&
    postModel[postsSqlModel.images].length > 0
  ) {
    const imagesData = postModel[postsSqlModel.images].map((e) => ({
      [imagesModel.url]: e[imagesModel.url],
      [mysqlCommonModel.postId]: posts[mysqlCommonModel.id],
    }));

    promises.push(brokerDb[mySqlDbTables.images].bulkCreate(imagesData));
  }

  promises.push(sendFcmNotificationFn('New Post', `${isAdmin ? 'Admin' : 'Broker'} ${brokerName} has added new "${postModel[postsSqlModel.name]}" >>`, 'admin'))

  await Promise.all(promises);

  return utils.SuccessMsg(res, {});
};

export const editPost = async (req, res) => {
  const { postModel } = req.body;
  console.log(req.body);

  const promises = [];
  promises.push(
    brokerDb[mySqlDbTables.posts].update(postModel, {
      where: {
        [mysqlCommonModel.id]: postModel[mysqlCommonModel.id],
      },
    })
  );

  /// jobs
  if (postModel[postsSqlModel.type] === postType.jobs && postModel["job"].id) {
    promises.push(
      brokerDb[mySqlDbTables.jobs].update(postModel["job"], {
        where: {
          [mysqlCommonModel.id]: postModel["job"].id,
        },
      })
    );
  }

  ///rentHouse,saleHouse
  if (
    (postModel[postsSqlModel.type] === postType.rentHouse ||
      postModel[postsSqlModel.type] === postType.saleHouse || postModel[postsSqlModel.type] === postType.realState) &&
    postModel["house"].id
  ) {
    promises.push(
      brokerDb[mySqlDbTables.houses].update(postModel["house"], {
        where: {
          [mysqlCommonModel.id]: postModel["house"].id,
        },
      })
    );
  }

  /// saleCar,rentCar
  if (
    (postModel[postsSqlModel.type] === postType.saleCar || postModel[postsSqlModel.type] === postType.rentCar) &&
    postModel["car"].id
  ) {
    promises.push(
      brokerDb[mySqlDbTables.cars].update(postModel["car"], {
        where: {
          [mysqlCommonModel.id]: postModel["car"].id,
        },
      })
    );
  }


   /// tenders
   if (
    postModel[postsSqlModel.type] === postType.tenders &&
    postModel["tender"].id
  ) {
      if(postModel.tender[postsSqlModel.tenderType] === tenderType.house) {
  
        promises.push(brokerDb[mySqlDbTables.houses].update(
          postModel['house'],
          {
            where: {
              [mysqlCommonModel.id]: postModel["house"].id,
            },
          }
        ));
        
      } else if(postModel.tender[postsSqlModel.tenderType] === tenderType.car) {
  
        promises.push(brokerDb[mySqlDbTables.cars].update(postModel['car'], {
          where: {
            [mysqlCommonModel.id]: postModel["car"].id,
          },
        }));
  
      }
  


    promises.push(
      brokerDb[mySqlDbTables.tenders].update(postModel["tender"], {
        where: {
          [mysqlCommonModel.id]: postModel["tender"].id,
        },
      })

    );
  }

  /// realState
  // if (
  //   postModel[postsSqlModel.type] === postType.realState &&
  //   postModel["realState"].id
  // ) {
  //   promises.push(
  //     brokerDb[mySqlDbTables.realStates].update(postModel["realState"], {
  //       where: {
  //         [mysqlCommonModel.id]: postModel["realState"].id,
  //       },
  //     })
  //   );
  // }

  if (
    postModel[postsSqlModel.specification] &&
    postModel[postsSqlModel.specification].length > 0
  ) {
    const sql = [];
    let newSpecificationsList = [];

    postModel[postsSqlModel.specification].forEach((v) => {
      if (!v[mysqlCommonModel.id] || v[mysqlCommonModel.id] === "") {
        newSpecificationsList.push({
          [specificationModel.field]: v[specificationModel.field],
          [specificationModel.value]: v[specificationModel.value],
          [mysqlCommonModel.postId]: postModel[mysqlCommonModel.id],
        });
      } else {
        sql.push(
          `
          UPDATE ${mySqlDbTables.specifications} 
          SET ${specificationModel.field} = "${v[specificationModel.field]}",
          ${specificationModel.value} = "${v[specificationModel.value]}"
          WHERE ${mysqlCommonModel.id} = ${v[mysqlCommonModel.id]}
          `
        );
      }
    });

    if (newSpecificationsList && newSpecificationsList.length > 0) {
      promises.push(
        brokerDb[mySqlDbTables.specifications].bulkCreate(newSpecificationsList)
      );
    }

    if (sql && sql.length > 0) {
      promises.push(sequelize.query(sql.join(";")));
    }
  }

  if (
    postModel[postsSqlModel.images] &&
    postModel[postsSqlModel.images].length > 0
  ) {
    const sql = [];
    let newImagesList = [];

    postModel[postsSqlModel.images].forEach((v) => {
      if (!v[mysqlCommonModel.id] || v[mysqlCommonModel.id] === "") {
        newImagesList.push({
          [imagesModel.url]: v[imagesModel.url],
          [mysqlCommonModel.postId]: postModel[mysqlCommonModel.id],
        });
      } else {
        sql.push(
          `
          UPDATE ${mySqlDbTables.images} 
          SET ${imagesModel.url} = "${v[imagesModel.url]}"
          WHERE ${mysqlCommonModel.id} = ${v[mysqlCommonModel.id]}
          `
        );
      }
    });

    if (newImagesList && newImagesList.length > 0) {
      promises.push(brokerDb[mySqlDbTables.images].bulkCreate(newImagesList));
    }

    if (sql && sql.length > 0) {
      promises.push(sequelize.query(sql.join(";")));
    }
  }

  await Promise.all(promises);

  return utils.SuccessMsg(res, {});
};

export const getPaginatedTender = async (req, res) => {
  const { brokerId, page, currentPostFilter } = req.body;
  const { limits, skip } = optimizeQueryPagination(page);

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.images], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].hasMany(
    brokerDb[mySqlDbTables.specifications],
    {
      foreignKey: mysqlCommonModel.postId,
    }
  );

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.comments], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.tenders], {
    foreignKey: mysqlCommonModel.tenderId,
  });

  brokerDb[mySqlDbTables.comments].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  brokerDb[mySqlDbTables.tenders].belongsTo(brokerDb[mySqlDbTables.houses], {
    foreignKey: mysqlCommonModel.houseId,
  });
  brokerDb[mySqlDbTables.tenders].belongsTo(brokerDb[mySqlDbTables.cars], {
    foreignKey: mysqlCommonModel.carId,
  });

  let priceMinMax = {};

  let hasPrice = false;

  if (isDefAndNotEmpty(currentPostFilter.priceMin)) {
    hasPrice = true;
    priceMinMax[Op.gte] = Number(currentPostFilter.priceMin);
  }
  if (isDefAndNotEmpty(currentPostFilter.priceMax)) {
    hasPrice = true;
    priceMinMax[Op.lte] = Number(currentPostFilter.priceMax);
  }

  const opAnd = [];
  let tenderAnd = [];

  if (hasPrice) {
    opAnd.push({
      [postsSqlModel.price]: priceMinMax,
    });
  }


  if (isDefAndNotEmpty(currentPostFilter.condition)) {
    opAnd.push({
      [postsSqlModel.condition]: currentPostFilter.condition,
    });
  }

  if(isDefAndNotEmpty(currentPostFilter.tenderType)) {
    tenderAnd.push(
      {
        [postsSqlModel.tenderType]: currentPostFilter.tenderType
      }
    );
  }


  const data = await brokerDb[mySqlDbTables.posts].findAll({
    where: sequelize.and({
      // ...
      [postsSqlModel.type]: currentPostFilter.type,
      [Op.and]: [
        ...opAnd,
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.waiting,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.rejected,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.completed,
          },
        },
      ],
    }),
    include: [
      {
        model: brokerDb[mySqlDbTables.tenders],
        where: sequelize.and({
          [Op.and]: tenderAnd,
        }),
        include: [
          {
            model: brokerDb[mySqlDbTables.houses],
          },    
          {
            model: brokerDb[mySqlDbTables.cars],
          },    
        ]
      },
      {
        model: brokerDb[mySqlDbTables.users],
        attributes: userAttributes,
      },
      {
        model: brokerDb[mySqlDbTables.specifications],
      },
      {
        model: brokerDb[mySqlDbTables.images],
      },
      {
        model: brokerDb[mySqlDbTables.comments],
        where: {
          [mysqlCommonModel.brokerId]: brokerId,
        },
        include: [
          {
            model: brokerDb[mySqlDbTables.users],
            attributes: userAttributes,
          },
        ],
        required: false,
      },
    ],
    order: currentPostFilter.selectedSort || [],
    limit: limits,
    offset: skip,
  });

  return utils.SuccessMsg(res, { data });
};

export const getPaginatedWhatPeopleWant = async (req, res) => {
  const { brokerId, page, currentPostFilter } = req.body;
  const { limits, skip } = optimizeQueryPagination(page);

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.images], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].hasMany(
    brokerDb[mySqlDbTables.specifications],
    {
      foreignKey: mysqlCommonModel.postId,
    }
  );

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.comments], {
    foreignKey: mysqlCommonModel.postId,
  });


  brokerDb[mySqlDbTables.comments].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.houses], {
    foreignKey: mysqlCommonModel.houseId,
  });
  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.cars], {
    foreignKey: mysqlCommonModel.carId,
  });

  let priceMinMax = {};

  let hasPrice = false;

  if (isDefAndNotEmpty(currentPostFilter.priceMin)) {
    hasPrice = true;
    priceMinMax[Op.gte] = Number(currentPostFilter.priceMin);
  }
  if (isDefAndNotEmpty(currentPostFilter.priceMax)) {
    hasPrice = true;
    priceMinMax[Op.lte] = Number(currentPostFilter.priceMax);
  }

  const opAnd = [];

  if (hasPrice) {
    opAnd.push({
      [postsSqlModel.price]: priceMinMax,
    });
  }


  if (isDefAndNotEmpty(currentPostFilter.condition)) {
    opAnd.push({
      [postsSqlModel.condition]: currentPostFilter.condition,
    });
  }

  if(isDefAndNotEmpty(currentPostFilter.tenderType)) {
    opAnd.push(
      {
        [postsSqlModel.type]: currentPostFilter.tenderType
      }
    );
  }



  const data = await brokerDb[mySqlDbTables.posts].findAll({
    where: sequelize.and({
      // ...
      [postsSqlModel.isIWant]: true,
      [Op.and]: [
        ...opAnd,
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.waiting,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.rejected,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.completed,
          },
        },
      ],
    }),
    include: [
      {
        model: brokerDb[mySqlDbTables.houses],
      },    
      {
        model: brokerDb[mySqlDbTables.cars],
      },    
      {
        model: brokerDb[mySqlDbTables.users],
        attributes: userAttributes,
      },
      {
        model: brokerDb[mySqlDbTables.specifications],
      },
      {
        model: brokerDb[mySqlDbTables.images],
      },
      {
        model: brokerDb[mySqlDbTables.comments],
        where: {
          [mysqlCommonModel.brokerId]: brokerId,
        },
        include: [
          {
            model: brokerDb[mySqlDbTables.users],
            attributes: userAttributes,
          },
        ],
        required: false,
      },
    ],
    order: currentPostFilter.selectedSort || [],
    limit: limits,
    offset: skip,
  });

  return utils.SuccessMsg(res, { data });
};

export const getPaginatedRealState = async (req, res) => {
  const { brokerId, page, currentPostFilter } = req.body;
  const { limits, skip } = optimizeQueryPagination(page);

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.images], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].hasMany(
    brokerDb[mySqlDbTables.specifications],
    {
      foreignKey: mysqlCommonModel.postId,
    }
  );

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.comments], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.comments].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  let priceMinMax = {};

  let hasPrice = false;

  if (isDefAndNotEmpty(currentPostFilter.priceMin)) {
    hasPrice = true;
    priceMinMax[Op.gte] = Number(currentPostFilter.priceMin);
  }
  if (isDefAndNotEmpty(currentPostFilter.priceMax)) {
    hasPrice = true;
    priceMinMax[Op.lte] = Number(currentPostFilter.priceMax);
  }

  const opAnd = [];

  if (hasPrice) {
    opAnd.push({
      [postsSqlModel.price]: priceMinMax,
    });
  }


  // if (isDefAndNotEmpty(currentPostFilter.condition)) {
  //   opAnd.push({
  //     [postsSqlModel.condition]: currentPostFilter.condition,
  //   });
  // }


  const data = await brokerDb[mySqlDbTables.posts].findAll({
    where: sequelize.and({
      // ...
      [postsSqlModel.type]: currentPostFilter.type,
      [Op.and]: [
        ...opAnd,
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.waiting,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.rejected,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.completed,
          },
        },
      ],
    }),
    include: [
    
      {
        model: brokerDb[mySqlDbTables.users],
        attributes: userAttributes,
      },
      {
        model: brokerDb[mySqlDbTables.specifications],
      },
      {
        model: brokerDb[mySqlDbTables.images],
      },
      {
        model: brokerDb[mySqlDbTables.comments],
        where: {
          [mysqlCommonModel.brokerId]: brokerId,
        },
        include: [
          {
            model: brokerDb[mySqlDbTables.users],
            attributes: userAttributes,
          },
        ],
        required: false,
      },
    ],
    order: currentPostFilter.selectedSort || [],
    limit: limits,
    offset: skip,
  });

  return utils.SuccessMsg(res, { data });
};

export const getPaginatedCar = async (req, res) => {
  const { brokerId, page, currentPostFilter } = req.body;
  const { limits, skip } = optimizeQueryPagination(page);

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.images], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].hasMany(
    brokerDb[mySqlDbTables.specifications],
    {
      foreignKey: mysqlCommonModel.postId,
    }
  );

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.comments], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.cars], {
    foreignKey: mysqlCommonModel.carId,
  });

  brokerDb[mySqlDbTables.comments].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  let priceMinMax = {};
  let yearMinMax = {};
  let kilometerMinMax = {};

  let hasPrice = false;
  let hasYear = false;
  let hasKilometer = false;

  if (isDefAndNotEmpty(currentPostFilter.priceMin)) {
    hasPrice = true;
    priceMinMax[Op.gte] = Number(currentPostFilter.priceMin);
  }
  if (isDefAndNotEmpty(currentPostFilter.priceMax)) {
    hasPrice = true;
    priceMinMax[Op.lte] = Number(currentPostFilter.priceMax);
  }

  if (isDefAndNotEmpty(currentPostFilter.carMinYear)) {
    hasYear = true;
    yearMinMax[Op.gte] = Number(currentPostFilter.carMinYear);
  }

  if (isDefAndNotEmpty(currentPostFilter.carMaxYear)) {
    hasYear = true;
    yearMinMax[Op.lte] = Number(currentPostFilter.carMaxYear);
  }

  if (isDefAndNotEmpty(currentPostFilter.carMinKilometer)) {
    hasKilometer = true;
    kilometerMinMax[Op.gte] = Number(currentPostFilter.carMinKilometer);
  }

  if (isDefAndNotEmpty(currentPostFilter.carMaxKilometer)) {
    hasKilometer = true;
    kilometerMinMax[Op.lte] = Number(currentPostFilter.carMaxKilometer);
  }

  const opAnd = [];
  const carsAnd = [];
  const carsOr = [];
  let carFilter = {};

  if (
    currentPostFilter.carFuelType &&
    currentPostFilter.carFuelType.length > 0
  ) {
    currentPostFilter.carFuelType.forEach((element) => {
      carsOr.push({
        [postsSqlModel.carFuelType]: element,
      });
    });
  }

  if (hasPrice) {
    opAnd.push({
      [postsSqlModel.price]: priceMinMax,
    });
  }

  if (hasYear) {
    carsAnd.push({
      [postsSqlModel.carYear]: yearMinMax,
    });
  }
  if (hasKilometer) {
    carsAnd.push({
      [postsSqlModel.carMilage]: kilometerMinMax,
    });
  }

  if (isDefAndNotEmpty(currentPostFilter.condition)) {
    opAnd.push({
      [postsSqlModel.condition]: currentPostFilter.condition,
    });
  }


  carFilter[Op.and] = [
    ...carsAnd,
    carsOr && carsOr.length > 0
      ? {
          [Op.or]: [...carsOr],
        }
      : {},
  ];

  const data = await brokerDb[mySqlDbTables.posts].findAll({
    where: sequelize.and({
      // ...
      [postsSqlModel.type]: currentPostFilter.type,
      [postsSqlModel.isIWant]: false,
      [Op.and]: [
        ...opAnd,
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.waiting,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.rejected,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.completed,
          },
        },
      ],
    }),
    include: [
      {
        model: brokerDb[mySqlDbTables.cars],
        where: sequelize.and(carFilter),
      },
      {
        model: brokerDb[mySqlDbTables.users],
        attributes: userAttributes,
      },
      {
        model: brokerDb[mySqlDbTables.specifications],
      },
      {
        model: brokerDb[mySqlDbTables.images],
      },
      {
        model: brokerDb[mySqlDbTables.comments],
        where: {
          [mysqlCommonModel.brokerId]: brokerId,
        },
        include: [
          {
            model: brokerDb[mySqlDbTables.users],
            attributes: userAttributes,
          },
        ],
        required: false,
      },
    ],
    order: currentPostFilter.selectedSort || [],
    limit: limits,
    offset: skip,
  });

  return utils.SuccessMsg(res, { data });
};

export const getPaginatedHouse = async (req, res) => {
  const { brokerId, page, currentPostFilter } = req.body;
  const { limits, skip } = optimizeQueryPagination(page);

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.images], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].hasMany(
    brokerDb[mySqlDbTables.specifications],
    {
      foreignKey: mysqlCommonModel.postId,
    }
  );

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.comments], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.houses], {
    foreignKey: mysqlCommonModel.houseId,
  });

  brokerDb[mySqlDbTables.comments].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  let priceMinMax = {};
  let squareMinMax = {};
  let hasPrice = false;
  let hasSquare = false;

  if (isDefAndNotEmpty(currentPostFilter.priceMin)) {
    hasPrice = true;
    priceMinMax[Op.gte] = Number(currentPostFilter.priceMin);
  }
  if (isDefAndNotEmpty(currentPostFilter.priceMax)) {
    hasPrice = true;
    priceMinMax[Op.lte] = Number(currentPostFilter.priceMax);
  }

  if (isDefAndNotEmpty(currentPostFilter.houseMinSquare)) {
    hasSquare = true;
    squareMinMax[Op.gte] = Number(currentPostFilter.houseMinSquare);
  }
  if (isDefAndNotEmpty(currentPostFilter.houseMaxSquare)) {
    hasSquare = true;
    squareMinMax[Op.lte] = Number(currentPostFilter.houseMaxSquare);
  }

  const opAnd = [];
  const houseAnd = [];

  if (hasPrice) {
    opAnd.push({
      [postsSqlModel.price]: priceMinMax,
    });
  }

  if (hasSquare) {
    houseAnd.push({
      [postsSqlModel.square]: squareMinMax,
    });
  }

  if (isDef(currentPostFilter.houseMinBathroom)) {
    houseAnd.push({
      [postsSqlModel.houseBathroom]: {
        [Op.gte]: Number(currentPostFilter.houseMinBathroom),
      },
    });
  }

  if (isDef(currentPostFilter.houseMinBedroom)) {
    houseAnd.push({
      [postsSqlModel.houseBedroom]: {
        [Op.gte]: Number(currentPostFilter.houseMinBedroom),
      },
    });
  }

  if (isDefAndNotEmpty(currentPostFilter.condition)) {
    opAnd.push({
      [postsSqlModel.condition]: currentPostFilter.condition,
    });
  }
  
  const data = await brokerDb[mySqlDbTables.posts].findAll({
    where: sequelize.and({
      // ...
      [postsSqlModel.type]: currentPostFilter.type,
      [postsSqlModel.isIWant]: false,
      [Op.and]: [
        ...opAnd,
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.waiting,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.waiting,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.rejected,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.completed,
          },
        },
      ],
    }),
    include: [
      {
        model: brokerDb[mySqlDbTables.houses],
        where: sequelize.and({
          [Op.and]: houseAnd,
        }),
      },
      {
        model: brokerDb[mySqlDbTables.users],
        attributes: [userModelField.username, userModelField.phoneNumber, userModelField.type],
      },
      {
        model: brokerDb[mySqlDbTables.specifications],
      },
      {
        model: brokerDb[mySqlDbTables.images],
      },
      {
        model: brokerDb[mySqlDbTables.comments],
        where: {
          [mysqlCommonModel.brokerId]: brokerId,
        },
        include: [
          {
            model: brokerDb[mySqlDbTables.users],
            attributes: [userModelField.username, userModelField.phoneNumber, userModelField.type],
          },
        ],
        required: false,
      },
    ],
    order: currentPostFilter.selectedSort || [],
    limit: limits,
    offset: skip,
  });

  return utils.SuccessMsg(res, { data });
};

export const getPaginatedJob = async (req, res) => {
  const { brokerId, page, currentPostFilter } = req.body;
  const { limits, skip } = optimizeQueryPagination(page);

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.images], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].hasMany(
    brokerDb[mySqlDbTables.specifications],
    {
      foreignKey: mysqlCommonModel.postId,
    }
  );

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.comments], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.jobs], {
    foreignKey: mysqlCommonModel.jobId,
  });

  brokerDb[mySqlDbTables.comments].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  let priceMinMax = {};
  let jobFilter = [];
  let hasPrice = false;
  let jobEducationLevelOr = [];
  let jobWorkExperienceOr = [];
  let jobEmployeeTypeOr = [];

  if (
    currentPostFilter.jobEducationLevel &&
    currentPostFilter.jobEducationLevel.length > 0
  ) {
    currentPostFilter.jobEducationLevel.forEach((element) => {
      jobEducationLevelOr.push({
        [postsSqlModel.jobEducationLevel]: element,
      });
    });
  }

  if (
    currentPostFilter.jobWorkExperience &&
    currentPostFilter.jobWorkExperience.length > 0
  ) {
    currentPostFilter.jobWorkExperience.forEach((element) => {
      jobWorkExperienceOr.push({
        [postsSqlModel.jobWorkExperience]: element,
      });
    });
  }

  if (
    currentPostFilter.jobEmployeeType &&
    currentPostFilter.jobEmployeeType.length > 0
  ) {
    currentPostFilter.jobEmployeeType.forEach((element) => {
      jobEmployeeTypeOr.push({
        [postsSqlModel.jobEmployeeType]: element,
      });
    });
  }

  if (jobEducationLevelOr && jobEducationLevelOr.length > 0) {
    jobFilter.push({ [Op.or]: [...jobEducationLevelOr] });
  }
  if (jobWorkExperienceOr && jobWorkExperienceOr.length > 0) {
    jobFilter.push({ [Op.or]: [...jobWorkExperienceOr] });
  }
  if (jobEmployeeTypeOr && jobEmployeeTypeOr.length > 0) {
    jobFilter.push({ [Op.or]: [...jobEmployeeTypeOr] });
  }

  if (isDefAndNotEmpty(currentPostFilter.priceMin)) {
    hasPrice = true;
    priceMinMax[Op.gte] = Number(currentPostFilter.priceMin);
  }
  if (isDefAndNotEmpty(currentPostFilter.priceMax)) {
    hasPrice = true;
    priceMinMax[Op.lte] = Number(currentPostFilter.priceMax);
  }

  const opAnd = [];

  if (hasPrice) {
    opAnd.push({
      [postsSqlModel.price]: priceMinMax,
    });
  }

  if (isDefAndNotEmpty(currentPostFilter.condition)) {
    opAnd.push({
      [postsSqlModel.condition]: currentPostFilter.condition,
    });
  }

  const data = await brokerDb[mySqlDbTables.posts].findAll({
    where: sequelize.and({
      // ...
      [postsSqlModel.type]: currentPostFilter.type,
      [Op.and]: [
        ...opAnd,
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.waiting,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.rejected,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.completed,
          },
        },
      ],
    }),
    include: [
      {
        model: brokerDb[mySqlDbTables.jobs],
        where: sequelize.and({
          [Op.and]: [...jobFilter],
        }),
      },
      {
        model: brokerDb[mySqlDbTables.users],
        attributes: userAttributes,
      },
      {
        model: brokerDb[mySqlDbTables.specifications],
      },
      {
        model: brokerDb[mySqlDbTables.images],
      },
      {
        model: brokerDb[mySqlDbTables.comments],
        where: {
          [mysqlCommonModel.brokerId]: brokerId,
        },
        include: [
          {
            model: brokerDb[mySqlDbTables.users],
            attributes: userAttributes,
          },
        ],
        required: false,
      },
    ],
    order: currentPostFilter.selectedSort || [],
    limit: limits,
    offset: skip,
  });

  return utils.SuccessMsg(res, { data });
};

export const getPaginatedPost = async (req, res) => {
  const { currentPostFilter } = req.body;
  console.log(req.body);

  switch (currentPostFilter.type) {
    case postType.saleCar:
      getPaginatedCar(req, res);
      break;
    case postType.rentCar:
      getPaginatedCar(req, res);
      break;
    case postType.saleHouse:
      getPaginatedHouse(req, res);
      break;
    case postType.rentHouse:
      getPaginatedHouse(req, res);
      break;
    case postType.jobs:
      getPaginatedJob(req, res);
      break;
    case postType.tenders:
      getPaginatedTender(req, res);
      break;
    case postType.realState:
      getPaginatedHouse(req, res);
      break;
  case postType.whatIWant:
    getPaginatedWhatPeopleWant(req, res);
      break;
    default:
      utils.ErrMsg(res, {});
      break;
  }
};

export const getPaginatedSearchPost = async (req, res) => {
  const { page, q, type, brokerId } = req.body;
  const { limits, skip } = optimizeQueryPagination(page);
  console.log(req.body);
  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.images], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].hasMany(
    brokerDb[mySqlDbTables.specifications],
    {
      foreignKey: mysqlCommonModel.postId,
    }
  );

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.comments], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.cars], {
    foreignKey: mysqlCommonModel.carId,
  });
  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.houses], {
    foreignKey: mysqlCommonModel.houseId,
  });
  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.jobs], {
    foreignKey: mysqlCommonModel.jobId,
  });

  // tender start
  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.tenders], {
    foreignKey: mysqlCommonModel.tenderId,
  });

  brokerDb[mySqlDbTables.tenders].belongsTo(brokerDb[mySqlDbTables.houses], {
    foreignKey: mysqlCommonModel.houseId,
  });
  brokerDb[mySqlDbTables.tenders].belongsTo(brokerDb[mySqlDbTables.cars], {
    foreignKey: mysqlCommonModel.carId,
  });

  // tender end


  brokerDb[mySqlDbTables.comments].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  let otherIncludes = [];
  let otherAnds = [];

  if(type) {
    if(type === postType.whatIWant) {
      otherAnds.push({[postsSqlModel.isIWant]: true});
    } else {
      otherAnds.push({[postsSqlModel.isIWant]: false});
      otherAnds.push({[postsSqlModel.type]: type});
    }
  }

  if(type=== postType.saleCar || type=== postType.rentCar || type=== postType.whatIWant) {
    otherIncludes.push({
      model: brokerDb[mySqlDbTables.cars],
    })
  } 
  if(type=== postType.rentHouse || type=== postType.saleHouse || type=== postType.realState || type=== postType.whatIWant) {
    otherIncludes.push({
      model: brokerDb[mySqlDbTables.houses],
    })
  }
  if(type=== postType.jobs) {
    otherIncludes.push({
      model: brokerDb[mySqlDbTables.jobs],
    })
  }else if(type=== postType.tenders) {
    otherIncludes.push({
      model: brokerDb[mySqlDbTables.tenders],
      include: [
        {
          model: brokerDb[mySqlDbTables.houses],
        },    
        {
          model: brokerDb[mySqlDbTables.cars],
        },    
      ]
    })
  } else {
    otherIncludes.push({
      model: brokerDb[mySqlDbTables.jobs],
    },{
      model: brokerDb[mySqlDbTables.houses],
    },{
      model: brokerDb[mySqlDbTables.cars],
    },{
      model: brokerDb[mySqlDbTables.tenders],
      include: [
        {
          model: brokerDb[mySqlDbTables.houses],
        },    
        {
          model: brokerDb[mySqlDbTables.cars],
        },    
      ]
    })
  }


  const data = await brokerDb[mySqlDbTables.posts].findAll({
    where: sequelize.and({
      // ...
      [Op.and]: [
        ...otherAnds,
        {
          [Op.or]: {
            [postsSqlModel.name]: {
              [Op.like]: `%${q}%`
            },
            [postsSqlModel.desc]: {
              [Op.like]: `%${q}%`
            },
            [postsSqlModel.location]: {
              [Op.like]: `%${q}%`
            },
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.waiting,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.rejected,
          },
        },
        {
          [postsSqlModel.status]: {
            [Op.ne]: postStatus.completed,
          },
        },
      ],
    }),
    include: [
      ...otherIncludes,
      {
        model: brokerDb[mySqlDbTables.users],
        attributes: userAttributes,
      },
      {
        model: brokerDb[mySqlDbTables.specifications],
      },
      {
        model: brokerDb[mySqlDbTables.images],
      },
      {
        model: brokerDb[mySqlDbTables.comments],
        where: {
          [mysqlCommonModel.brokerId]: brokerId,
        },
        include: [
          {
            model: brokerDb[mySqlDbTables.users],
            attributes: userAttributes,
          },
        ],
        required: false,
      },
    ],
    limit: limits,
    offset: skip,
  });

  return utils.SuccessMsg(res, { data });
};

export const submitComment = async (req, res) => {
  const { commentModel, postModel, username } = req.body;
  console.log(commentModel);
  let data;
  const body = commentModel[mysqlCommonModel.id] ? `Broker ${username} has changed the price to ${commentModel[commentsModel.price]} For "${
    postModel[postsSqlModel.name]
  }"` : `I have a buyer who is willing to pay for "${
    postModel[postsSqlModel.name]
  }" by ${commentModel[commentsModel.price]}`;

  if (commentModel[mysqlCommonModel.id]) {
    data = await brokerDb[mySqlDbTables.comments].update(commentModel, {
      where: {
        [mysqlCommonModel.id]: commentModel[mysqlCommonModel.id],
      },
    });
  } else {
    data = await brokerDb[mySqlDbTables.comments].create(commentModel);
  }
  let notifications = {
    [notificationField.body]: body,
    [notificationField.title]: username || "",
    [notificationField.icon]: postModel[postsSqlModel.type],
    [mysqlCommonModel.brokerId]: commentModel[mysqlCommonModel.brokerId],
    [mysqlCommonModel.postId]: postModel[mysqlCommonModel.id],
  };
  res.locals.notificationData = [notifications];
  const promises = [];
  promises.push(saveNotification(req, res));
  promises.push(sendFcmNotificationFn('Comment', body, 'admin'))
  await Promise.all(promises);
  /// send fcm pontification
  if(data) {
    return utils.SuccessMsg(res, {data})
  } else {
    return utils.ErrMsg(res, {})
  }
};

