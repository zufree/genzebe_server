import utils from "../../core/processor";
import { optimizeQueryPagination } from "../../global/routeHelpers";
import brokerDb from "../../models";
import {
  postsSqlModel,
  userModelField,
  userStatus,
  userType
} from "../../models/field_model";
import { mysqlCommonModel, mySqlDbTables } from "../../models/mysql_db_table";
import { isDefAndNotEmpty, timeAgo } from "../../utils/helpers";
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
export const loginUser = async (req, res) => {
  const { userModel, broker } = req.body;

  let respond;
  let isNewUser = false;

  const isUserExist = await brokerDb[mySqlDbTables.users].findOne({
    where: {
      [mysqlCommonModel.brokerId]: userModel[mysqlCommonModel.brokerId],
    },
  });

  isNewUser = isUserExist ? true : false;

  if (isUserExist) {
    await brokerDb[mySqlDbTables.users].update(
      {
        [userModelField.token]: userModel[userModelField.token],
        [userModelField.platform]: userModel[userModelField.platform],
        // [userModelField.type]: broker ? userType.broker : userType.customer,
      },
      {
        where: {
          [mysqlCommonModel.brokerId]: userModel[mysqlCommonModel.brokerId],
        },
        returning: true,
      }
    );
    respond = await brokerDb[mySqlDbTables.users].findOne({
      where: {
        [mysqlCommonModel.brokerId]: userModel[mysqlCommonModel.brokerId],
      },
    });
  } else {
    respond = await brokerDb[mySqlDbTables.users].create({
      [mysqlCommonModel.brokerId]: userModel[mysqlCommonModel.brokerId],
      [userModelField.phoneNumber]: userModel[userModelField.phoneNumber],
      [userModelField.type]: broker ? userType.broker : userType.customer,
      [userModelField.token]: userModel[userModelField.token],
      [userModelField.username]: userModel[userModelField.username],
      [userModelField.status]: userStatus.waiting,
      [userModelField.platform]: userModel[userModelField.platform],
    });
  }

  return respond
    ? utils.SuccessMsg(res, { data: { respond, isNewUser } })
    : utils.ErrMsg(res, {});
};

// export const getNotifications = async (req, res) => {
//   const { brokerId, page } = req.body;
//   const { limits, skip } = optimizeQueryPagination(page);
//   const data = await brokerDb[mySqlDbTables.notifications].findAll({
//     where: {
//       [notificationField.brokerId]: brokerId,
//     },
//     order: [[mysqlCommonModel.createdAt, "DESC"]],
//     raw: true,
//     limit: limits,
//     offset: skip,
//   });
//   data.forEach((element) => {
//     element.date = timeAgo(new Date(element.createdAt));
//   });
//   return utils.SuccessMsg(res, { data });
// };

export const getProfileInfo = async (req, res) => {
  const { brokerId } = req.body;

  
  const data = await brokerDb[mySqlDbTables.users].findOne({
    where: {
      [mysqlCommonModel.id]: brokerId,
    },
  });
  
  return utils.SuccessMsg(res, {
    data,
  });
};

export const editProfile = async (req, res) => {
  const { userModel } = req.body;

  await brokerDb[mySqlDbTables.users].update(
    {
      [userModelField.username]: userModel[userModelField.username],
    },
    {
      where: {
        [mysqlCommonModel.id]: userModel[mysqlCommonModel.id],
      },
    }
  );

  return utils.SuccessMsg(res, {});
};



export const getMySubmittedPosts = async (req, res) => {
  const { brokerId, page, type, status } = req.body;

  const { limits, skip } = optimizeQueryPagination(page);

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.images], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.specifications], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.comments], {
    foreignKey: mysqlCommonModel.postId,
  });
  
  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.jobs], {
    foreignKey: mysqlCommonModel.jobId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.cars], {
    foreignKey: mysqlCommonModel.carId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.houses], {
    foreignKey: mysqlCommonModel.houseId,
  });


  /// tenders
  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.tenders], {
    foreignKey: mysqlCommonModel.tenderId,
  });

  brokerDb[mySqlDbTables.tenders].belongsTo(brokerDb[mySqlDbTables.houses], {
    foreignKey: mysqlCommonModel.houseId,
  });
  brokerDb[mySqlDbTables.tenders].belongsTo(brokerDb[mySqlDbTables.cars], {
    foreignKey: mysqlCommonModel.carId,
  });
  

  let where = {};
  where[mysqlCommonModel.brokerId] = brokerId;
  
  if(isDefAndNotEmpty(type)) {
    where[postsSqlModel.type] = type;
  }
  
  if(isDefAndNotEmpty(status)) {
    where[postsSqlModel.status] = status;
  }

  const data = await brokerDb[mySqlDbTables.posts].findAll({
    where,
    include: [
      {
        model: brokerDb[mySqlDbTables.specifications],
      },
      {
        model: brokerDb[mySqlDbTables.houses],
      },
      {
        model: brokerDb[mySqlDbTables.jobs],
      },
      {
        model: brokerDb[mySqlDbTables.cars],
      },
      {
        model: brokerDb[mySqlDbTables.tenders],
        include: [
          {
            model: brokerDb[mySqlDbTables.houses],
          },    
          {
            model: brokerDb[mySqlDbTables.cars],
          },    
        ]
      },
      
      {
        model: brokerDb[mySqlDbTables.images],
      },
      // {
      //   model: brokerDb[mySqlDbTables.comments],
      //   where: {
      //     [mysqlCommonModel.brokerId]: brokerId,
      //   },
      // },
    ],
    order: [[mysqlCommonModel.createdAt, 'DESC']],
    limit: limits,
    offset: skip,
  });

  if(data && data.length > 0) {
    data.forEach((element) => {
      element.setDataValue('timeAgo', timeAgo(new Date(element[mysqlCommonModel.createdAt])));
    });
  }
  return utils.SuccessMsg(res, {data});
};
