import utils from "../../core/processor";
import { optimizeQueryPagination } from "../../global/routeHelpers";
import brokerDb from "../../models";
import {
  postsSqlModel,
  postType,
  userModelField,
  userType
} from "../../models/field_model";
import { mysqlCommonModel, mySqlDbTables } from "../../models/mysql_db_table";
import { isDefAndNotEmpty, timeAgo } from "../../utils/helpers";
import { userAttributes } from './posts_ctr';

const {
  firebaseAdmin,
} = require("../../config/firebase.config")


const Sequelize = require("sequelize");
const Op = Sequelize.Op;

export const changePostStatus = async (req, res) => {
  const { postId, status } = req.body;

  await brokerDb[mySqlDbTables.posts].update(
    {
      [postsSqlModel.status]: status,
    },
    {
      where: {
        [mysqlCommonModel.id]: postId,
      },
    }
  );

  utils.SuccessMsg(res, {});
};

// TODO delete associated data
export const deletePost = async (req, res) => {
  const { postId } = req.body;

  await brokerDb[mySqlDbTables.posts].destroy({
    where: {
      [mysqlCommonModel.id]: postId,
    },
  });

  utils.SuccessMsg(res, {});
};

// TODO exclude
export const getAllUsers = async (req, res) => {
  const { status, page } = req.body;
  const { limits, skip } = optimizeQueryPagination(page);
  console.log(req.body);
  let data;
  let where = {};

  if (status) {
    where[userModelField.status] = status;
  }

  where[Op.and] = [
    {
      [userModelField.type]: {
        [Op.ne]: userType.admin,
      }
    },
    {
      [userModelField.type]: {
        [Op.ne]: userType.customer,
      }
    }
  ]

  

  data = await brokerDb[mySqlDbTables.users].findAll({
    where: Sequelize.and(where),
    limit: limits,
    offset: skip,
  });

  utils.SuccessMsg(res, { data });
};

export const changeUsersStatus = async (req, res) => {
  const { status, userId } = req.body;

  await brokerDb[mySqlDbTables.users].update(
    {
      [userModelField.status]: status,
    },
    {
      where: {
        [mysqlCommonModel.id]: userId,
      },
    }
  );

  utils.SuccessMsg(res, {});
};

export const deleteUser = async (req, res) => {
  const { userId } = req.body;

  await brokerDb[mySqlDbTables.users].destroy({
    where: {
      [mysqlCommonModel.id]: userId,
    },
  });

  utils.SuccessMsg(res, {});
};

export const getAllPosts = async (req, res) => {
  const { type, page, status, } = req.body;
  console.log(req.body);

  const { limits, skip } = optimizeQueryPagination(page);

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.images], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].hasMany(
    brokerDb[mySqlDbTables.specifications],
    {
      foreignKey: mysqlCommonModel.postId,
    }
  );

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.comments], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.jobs], {
    foreignKey: mysqlCommonModel.jobId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.cars], {
    foreignKey: mysqlCommonModel.carId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.houses], {
    foreignKey: mysqlCommonModel.houseId,
  });
  
  /// tenders
  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.tenders], {
    foreignKey: mysqlCommonModel.tenderId,
  });

  brokerDb[mySqlDbTables.tenders].belongsTo(brokerDb[mySqlDbTables.houses], {
    foreignKey: mysqlCommonModel.houseId,
  });

  brokerDb[mySqlDbTables.tenders].belongsTo(brokerDb[mySqlDbTables.cars], {
    foreignKey: mysqlCommonModel.carId,
  });



  brokerDb[mySqlDbTables.comments].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });
  
  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });



  let where = {};
  if(isDefAndNotEmpty(type)) {
    if(type == postType.whatIWant) {

      where[postsSqlModel.isIWant] = true;
    } else {

      where[postsSqlModel.type] = type;
    }
  } 
  if (isDefAndNotEmpty(status)) {
    where[postsSqlModel.status] = status;
  }

  const data = await brokerDb[mySqlDbTables.posts].findAll({
    where,
    include: [
      {
        model: brokerDb[mySqlDbTables.specifications],
      },
      {
        model: brokerDb[mySqlDbTables.users],
        attributes: userAttributes,
      },
      {
        model: brokerDb[mySqlDbTables.houses],
      },
      {
        model: brokerDb[mySqlDbTables.jobs],
      },
      {
        model: brokerDb[mySqlDbTables.tenders],
        include: [
          {
            model: brokerDb[mySqlDbTables.houses],
          },    
          {
            model: brokerDb[mySqlDbTables.cars],
          },    
        ]
      },
      
      {
        model: brokerDb[mySqlDbTables.comments],
        include: [
          {
            model: brokerDb[mySqlDbTables.users],
            attributes: userAttributes,
          },
        ],
      },
      {
        model: brokerDb[mySqlDbTables.cars],
      },
      {
        model: brokerDb[mySqlDbTables.images],
      },
    ],
    order: [[mysqlCommonModel.createdAt, "DESC"]],
    limit: limits,
    offset: skip,
  });

  return utils.SuccessMsg(res, { data });
};

export const getAllNotification = async (req, res) => {
  const { page } = req.body;

  const { limits, skip } = optimizeQueryPagination(page);

  brokerDb[mySqlDbTables.notifications].belongsTo(
    brokerDb[mySqlDbTables.posts],
    {
      foreignKey: mysqlCommonModel.postId,
    }
  );

  brokerDb[mySqlDbTables.notifications].belongsTo(
    brokerDb[mySqlDbTables.users],
    {
      foreignKey: mysqlCommonModel.brokerId,
    }
  );

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.images], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].hasMany(
    brokerDb[mySqlDbTables.specifications],
    {
      foreignKey: mysqlCommonModel.postId,
    }
  );

  brokerDb[mySqlDbTables.posts].hasMany(brokerDb[mySqlDbTables.comments], {
    foreignKey: mysqlCommonModel.postId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.jobs], {
    foreignKey: mysqlCommonModel.jobId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.cars], {
    foreignKey: mysqlCommonModel.carId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.houses], {
    foreignKey: mysqlCommonModel.houseId,
  });

  /// tenders
  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.tenders], {
    foreignKey: mysqlCommonModel.tenderId,
  });

  brokerDb[mySqlDbTables.tenders].belongsTo(brokerDb[mySqlDbTables.houses], {
    foreignKey: mysqlCommonModel.houseId,
  });

  brokerDb[mySqlDbTables.tenders].belongsTo(brokerDb[mySqlDbTables.cars], {
    foreignKey: mysqlCommonModel.carId,
  });

  brokerDb[mySqlDbTables.comments].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  brokerDb[mySqlDbTables.posts].belongsTo(brokerDb[mySqlDbTables.users], {
    foreignKey: mysqlCommonModel.brokerId,
  });

  const data = await brokerDb[mySqlDbTables.notifications].findAll({
    include: [
      {
        model: brokerDb[mySqlDbTables.users],
        attributes: userAttributes,
      },
      {
        model: brokerDb[mySqlDbTables.posts],
        include: [
          {
            model: brokerDb[mySqlDbTables.specifications],
          },
          {
            model: brokerDb[mySqlDbTables.houses],
          },
          {
            model: brokerDb[mySqlDbTables.jobs],
          },
          {
            model: brokerDb[mySqlDbTables.cars],
          },
          {
            model: brokerDb[mySqlDbTables.tenders],
            include: [
              {
                model: brokerDb[mySqlDbTables.houses],
              },    
              {
                model: brokerDb[mySqlDbTables.cars],
              },    
            ]
          },
          {
            model: brokerDb[mySqlDbTables.users],
            attributes: userAttributes,
          },
          {
            model: brokerDb[mySqlDbTables.comments],
            include: [
              {
                model: brokerDb[mySqlDbTables.users],
                attributes: userAttributes,
              },
            ],
          },
          {
            model: brokerDb[mySqlDbTables.images],
          },
        ],
      },
    ],
    order: [[mysqlCommonModel.createdAt, "DESC"]],
    limit: limits,
    offset: skip,
  });

  if (data && data.length > 0) {
    data.forEach((element) => {
      element.setDataValue(
        "timeAgo",
        timeAgo(new Date(element[mysqlCommonModel.createdAt]))
      );
    });
  }

  return utils.SuccessMsg(res, { data });
};

export const  sendFcmNotificationFn  = async (title, desc, topic) => {
  const fcm = firebaseAdmin.messaging();
  const payload = {
    notification: {
      title: title || "New notification",
      body: desc || "",
      icon: "",
      click_action: "FLUTTER_NOTIFICATION_CLICK",
    },
    data: {
      'hello': 'there',
    },
  };
  await fcm.sendToTopic(topic, payload)
}

export const sendFcmNotification = async (req, res) => {
  const {
    title,
    desc,
  } = req.body;
  try {
    await sendFcmNotificationFn(title, desc, 'all');
  } catch (error) {
    return utils.ErrMsg(res, {});
  }
  return utils.SuccessMsg(res, {});
};
