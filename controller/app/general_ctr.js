import brokerDb from "../../models";
import { mySqlDbTables } from "../../models/mysql_db_table";

export const saveNotification = async (req, res) => {
  const notificationData = res.locals.notificationData;
  
  await brokerDb[mySqlDbTables.notifications].bulkCreate(notificationData);
  res.locals.notificationData = [];
  // return utils.SuccessMsg(res, {})
}